#!/bin/sh

while true; do
	# make pictures variable empty
	pictures=
	# fill pictures variable with a list of files, randomly sorted
	pictures=$(ls Pictures/ | sort -R)

	for item in $pictures; do	# loop through files
		clear			# clear screen
		viu Pictures/$item	# display image with viu
		sleep 2			# wait 2 seconds
	done

done
