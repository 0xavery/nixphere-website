<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="assets/css/skeleton.css">
		<link rel="stylesheet" href="assets/css/nixphere.css">
		<link rel="stylesheet" href="assets/css/normalize.css">
		<link rel="stylesheet" href="assets/forkawesome/css/fork-awesome.min.css">
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="twelve columns">
					<h2>nixphere.org</h2>
					<p>Password authentication has been disabled. Use ssh keys please.</p>
					<p><a href="https://nixphere.org/bb">Server News </a> </p>
				</div>
			</div>
			<!--<img src="assets/imgs/nixphere.svg">-->

			<div class="row">
				<div class="twelve columns">
					<blockquote>
						<em> It's broken but it's the lest broken thing I've seen in a long time. </em>
						<br>
						<br>
						<img src="assets/imgs/fire.jpg">
					</blockquote>
				</div>
			</div>

			<div class="row">
				<div class="twelve columns">
					<h2> Services on nixphere </h2>
				</div>
			</div>

			<div class="row">
				<div class="six columns">
					<h4> <i class="fa fa-terminal" aria-hidden="true"></i> <a href="https://nixphere.org">Shell Server </a> </h4>
				</div>	
				<div class="six columns">
					<p> Nixphere provides a FreeBSD terminal environment pre-loaded with many things. Lanuages, compilers, and runtimes include Python, Erlang, Lua, R, LLVM, R, and Shell. In order to avoid resource hogging, each user is restricted to a 1G ZFS dataset and FreeBSD login profiles are used for thier process limiting capabilities. It's not that I don't trust the users, it's that I want to prevent accidental foot shooting. More resources are available on request. 
					</p>
				</div>	
			</div>


			<div class="row">
				<div class="six columns">
					<h4> <i class="fa fa-server" aria-hidden="true"></i> <a href="https://nixphere.org">Web Hosting</a> </h4>
				</div>	
				<div class="six columns">
					<p> Currently, there is only PHP but no database. In the future I plan to add MySQL. It's not hard, I'm just lazy. I have installed hundreds of FAMP stacks and to be honest, there are many other cool things I would rather do. Right now, static hosting is good enough. But I will add the M to FAMP if a user requests it. 
					</p>
				</div>	
			</div>


			<div class="row">
				<div class="six columns">
					<h4><i class="fa fa-comments-o" aria-hidden="true"></i> <a href="https://nixphere.org/bb">Bulletin Board</a> </h4>
				</div>	
				<div class="six columns">
					<p> Nixphere has a bulletin board system written in POSIX shell. It's viewable from the web and terminal but only writable from the terminal. Currently there is no image support but I'm working on it. 
					</p>
				</div>	
			</div>



			<div class="row">
				<div class="six columns">
					<h4> <i class="fa fa-bar-chart" aria-hidden="true"></i> <a href="https://r.nixphere.org">RStudio Server</a> </h4>
				</div>
				<div class="six columns">
					<p> We have no need for spreadsheets with RStudio. Although command line only R is great, it quickly becomes limiting when we need to visualize data. RStudio provides an familiar, web based interface to an R environment. 
					</p>
				</div>
			</div>

			<div class="row">
				<div class="six columns">
					<h4><i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="https://mail.nixphere.org"> Email </a></h4>
				</div>
				<div class="six columns">
					<p> In order to migrate from Google, I hosted my own email server and invited some of my friends. Not all nixphere users have an email account because big hosting providers are quick to ban servers not owned by the big email oligopoly. It's powered by OpenBSD like all reliable things are.
					</p>
				</div>
			</div>


			<div class="row">
				<div class="six columns">
					<h4><i class="fa fa-pleroma" aria-hidden="true"></i> <a href="https://pleroma.nixphere.org"> <s>Pleroma</s> </a> </h4>
				</div>
				<div class="six columns">
					<p> <s>Pleroma might not be running. Admin remembered all the reasons she left the feidverse. Admin also got tired of constantly babysitting pleroma. If pleroma is not up and you want it up, email me. Otherwise it will remain in an unreliable, zombified state.</s> <br> <b> killed in favor of gopher </b>
					</p>
				</div>
			</div>

			<div class="row">
				<div class="six columns">
					<h4><i class="fa fa-xmpp" aria-hidden="true"></i> <a href="https://xmpp.nixphere.org"> XMPP </a></h4>
				</div>
				<div class="six columns">
					<p> Useful because signal requires a phone number. XMPP might feel old, but using a client that supports OMEMO will make it just as secure as signal. </p>
				</div>
			</div>

			<div class="row">
				<div class="six columns">
					<h4><i class="fa fa-gitea" aria-hidden="true"></i> <a href="https://git.nixphere.org"> Git </a></h4>
				</div>
				<div class="six columns">
					<p> Nixphere's git server. Contains some of the server's custom scripts including bb (and all of it's backends), various scripts, and miscellaneous user projects. </p>
				</div>
			</div>

			<div class="row">
				<div class="six columns">
					<h4><i class="fa fa-file-text-o" aria-hidden="true"></i> <a href="gopher://nixphere.org:70/1"> Gopher </a></h4>
				</div>
				<div class="six columns">
					<p> SSL not included. </p>
				</div>
			</div>

			<div class="row">
				<div class="twelve columns">
					<h2> Registration </h2>
				</div>
				<div class="twelve columns">
					<p> <b>Currently closed.</b> In order to protect the system and the current userbase (and my wallet), only trusted people are allowed to register on the server. Open registration for all services is disabled in order to protect the system. We are invite only, but if you would like to register and have not received an invite, you can send an email to: <img  id="address" class="svgaddr" src="assets/imgs/addr.png">. Please include who you are, the services you want to join, why you want to join, and a current nixphere user who can vouch for your character. 
					</p>

				</div>
			</div>

			<div class="row">
				<div class="twelve columns">
					<h2> System Documentation </h2>
				</div>
				<div class="twelve columns">
					<p> Useful documentation to help new users get started: </p>
					<ul>

<?php
$docs = glob('docs/*.html');
$docs = array_reverse($docs);
foreach ($docs as $filename){
	$fp = file_get_contents($filename);
	if (!$fp)
		return null;

	$res = preg_match("/<title>(.*)<\/title>/siU", $fp, $title_matches);
	if (!$res)
		return null;

	// Clean up title: remove EOL's and excessive whitespace.
	$title = preg_replace('/\s+/', ' ', $title_matches[1]);
	$title = trim($title);
	echo "<li><a href='http://nixphere.org/" . $filename . "'>" . $title . "</a></li>";
}
?>
					</ul>

				</div>
			</div>


			<!--- <h2> Systemd refugees hiding out in kernel space: </h2> --->
			<div class="row">
				<div class="twelve columns">
					<h2> Canaries in the Coalmines </h2>
					<p> Users with web hosting on nixphere: </p>

					<ul>
<?php
$users = glob('~*');
foreach ($users as $filename){
	echo "<li><a href='http://nixphere.org/" . $filename . "'>" . $filename . "</a></li>";
}
?>
					</ul>
				</div>

			</div>
		</div>

<!--
		<div class="container">
			<div class="row">
				<div class="twelve columns">
					<img style="height: 36px;" src="assets/imgs/bsd.gif">
					<img style="height: 36px;" src="assets/imgs/mothracompat.gif">
				</div>
			</div>
		</div>
-->

<script>
// @license magnet:?xt=urn:btih:87f119ba0b429ba17a44b4bffcab33165ebdacc0&dn=freebsd.txt FreeBSD 
// swaps .png for .svg for better scaling on browsers that don't support css or js
document.getElementById("address").src="assets/imgs/addr.svg"
// @license-end
</script>

	</body>
</html>
