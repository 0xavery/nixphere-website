<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="/assets/css/skeleton.css">
		<link rel="stylesheet" href="/assets/css/nixphere.css">
		<link rel="stylesheet" href="/assets/css/normalize.css">
		<link rel="stylesheet" href="/assets/forkawesome/css/fork-awesome.min.css">
<style>
img{
display: block;
margin: 0 auto;
max-width: 60%;
}
</style>
	</head>
	<body>
		<div class="container">
			<div class="row">
				<div class="twelve columns">
				<h1> Nixphere's hellworld compilation </h1>
				</div>
			</div>

<?php

$imgs = glob('imgs/*');
foreach($imgs as $img){
	if(is_file($img)){
		echo "<div class='row'>";
		echo "<div class='twelve-columns'>";
		echo "<img src='" . $img . "'>";
		echo "</div></div>\n";
	}
}

?>
</div>
</body>
</html>

