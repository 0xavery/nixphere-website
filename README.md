# How
As the content is in markdown format, it can be exported easily in other formats.

Existing docs files have been kept in ```.html``` for legacy purposes

The new articles are in markdown format, in the docs folder.
This also avoids template and style duplication.

# Building
To launch a dev server:

```
zola serve
```

To build files, the output is in the ```public``` folder:

```
zola serve
```
