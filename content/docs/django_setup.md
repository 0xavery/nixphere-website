+++
title = "Django server setup"
extra.author = "celeste"
+++

Instructions for Fedora Server 34
# Webserver


## nginx
As simple as that
```
# dnf install nginx
```

## certbot
Considering www.example.com as our domain
```
# dnf install python-certbot-nginx
# certbot --nginx -d www.example.com
```

## proxy_pass


# Django
The django software is run by an internal server and using a lower privileged user.
This exchange data with _nginx_.

## Low privilege user creation
Create a _django_ user by typing
```
# useradd -m django
```

Set its password if you want to login there, useful for acting in its home directory files
```
# passwd user
```

If you're root, you can also login to this user with
```
# su -- django
```

## Python stuff
You need pip to proceed:
```# dnf install python3-pip```

### Virtualenv
To put shit into the global python installation, it's recommended to place a self-contained python interpeter in a folder. Local dependencies will be placed there too.

Make sure virtualenv creator is installed in your system. You can do it in two ways:
```# dnf install python3-virtualenv```
```# pip install virtualenv```

Login as django user, go in its home folder.

Create the virtualenv in a folder called _venv_ or how you like:
```
$ python3 -m venv venv
```

Activate it with:
```
$ source /venv/bin/activate
```
From this point, all _python_ commands will execute the virtualenv's interpreter and modules instead of the systemwide one.
If you run _pip install_ while in the virtualenv, dependencies will be placed inside it.

Deactivate with:
```
$ deactivate
```



### Django project config

Copy-paste how you want your django project folder next to the virtualenv one, for example placing it in the home folder too.
I'll use the simple sqlite driver for the database.

Make sure to have a requirements.txt file, listing all needed dependencies.
If you've a local working copy in yuor pc, you can generate this file with:
```
$ pip freeze > requirements.txt
```

After having copied the project, activate the _virtualenv_, _cd_ into the project folder and then run
```$ pip install -r requirements.txt```.
All dependencies will be installed automatically and resolved.

Now create all the database file and tables automatically with:
```
$ python manage.py makemigrations
$ python manage.py migrate
```

### Deploy checklist
For a production environment, make sure to apply these changes to your _settings.py_ file:

#### Secret signing key
Set a random SECRET_KEY used for signing sessions and cookies. Do NOT use the debug one. You can get it from an environment variable. Do **NOT** versions control it. Edit your project's settings.py file and add```
import os
SECRET_KEY = os.environ['SECRET_KEY']```

#### Debug mode
Disable debug mode to reduce the amount of information returned in responses in case of errors
```DEBUG = False```

#### Allowed hosts
Add your domain name here. This protects you from some CSRF attacks:
```
ALLOWED_HOSTS = ['yourdomain.com', 'www.yourdomain.com']
```

#### Database password
If you're using a non-sqlite database, make sure to also get the db password from an environment variable like you did with the SECRET_KEY.

#### Static files
Set STATIC_ROOT and STATIC_URL to tell django where to store static files that will be served by nginx/apache.

STATIC_ROOT has to be set to the path to the folder where static files will be copied and collected.
STATIC_URL tells django how to generate urls to point to static files. Normally it's just _/static/_. Point somewhere else if, for example, you're using a CDN.

Static files can be collected by running
```
$ python manage.py collectstatic
```

## Gunicorn
TO DO

## systemd
TO DO

## selinux
Just setting gunicorn to listen on port 9000 seems to overcome any SeLinux problems. SeLinux already allows communication on this port for network purposes.

## Firewall
TODO